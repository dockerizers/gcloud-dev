FROM google/cloud-sdk:alpine
LABEL maintainer="Kariuki Gathitu <kgathi2@gmail.com>"
LABEL version="1.0"

ENV VERSION 0.0.8

RUN apk --update add jq bash-completion
RUN gcloud components install kubectl gke-gcloud-auth-plugin
WORKDIR /gke
COPY ./entrypoint.sh entrypoint.private-cluster.sh bash_aliases install_krew.sh /tmp/
COPY ./bashrc /root/.bashrc
COPY ./kube_completion.bash /root/kube_completion.bash
RUN . /tmp/install_krew.sh
RUN . /tmp/bash_aliases
RUN chmod +x /tmp/bash_aliases /tmp/entrypoint.sh
RUN curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | VERIFY_CHECKSUM=false bash

ENTRYPOINT [ "/tmp/entrypoint.sh" ]
