[Docker hub](https://hub.docker.com/r/kgathi2/gcloud-dev)

# Setting up Gcloud K8 isolated project environment

This container is used to isolate gcloud environments within a project. Allows for multiple project work, safely at the same time.

The following components are installed;
- gcloud cloud-sdk
- kubectl
- [krew](https://krew.sigs.k8s.io/) - *The kubectl plugin manager*

## Set up you project

Project folder (app) should be peers with this.

```
|___ app
|___ gke
	|___ docker-compose.yaml
	|___ entrypoint.sh (optional for overriding Gcloud settup)
	|___ .gitignore
```

Setup your `docker-compose.yaml` file
```yaml
version: '3'

services:
  project_service:
    command: tail -f /dev/null
    image: kgathi2/gcloud-dev:latest
    volumes:
      - ./.gcloud:/.gcloud
      - ./:/gke
      # - ./entrypoint.sh:/tmp/entrypoint.sh # optional if overriding gcloud setup
    environment:
      - CLOUDSDK_CONFIG=/.gcloud
      - REGION=europe-west6
      - ZONE=europe-west6-a
      - PROJECT=ipsec-vpn-239415
      - CLUSTER_NAME=vpn-1
      - REGULAR_NODES=1
      - NODE_TYPE=e2-standard-2
      - NODE_IMAGE=cos_containerd # cos unsupported aas from k8s 1.24
      - CLUSTER_RELEASE_CHANNEL=regular
      - CLUSTER_MAINTENANCE_WINDOW="01:00"
      - ClUSTER_FLAGS=--enable-network-policy
        --something-else
      - PREEMPTIBLE_POOL_NAME=""
      - PREEMPTIBLE_NODES=2
      - PREEMPTIBLE_NODES_MAX=3


```
Ignore .gcloud  in `.gitignore`
```
.gcloud
```
Run command from the gke folder

```bash
$ cd gke
$ docker-compose run project_service bash
```
### Choosing a region
Your Kubernetes nodes should be located closest to your users for lowest latency. To check latency of Gcloud regions go to [gcping](http://www.gcping.com/) or [cloudpingtest](https://cloudpingtest.com/gcp). Also test latency [between GCP region](http://www.gcloudping.com/)
europe-west6 (Zürich, Switzerland a,b,c) is fastest
europe-west1 (St. Ghislain, Belgium b,c,d) has more features

### Choosing a Node type
GCP have various [machine types](https://cloud.google.com/compute/docs/machine-types) and corresponding [pricing](https://cloud.google.com/compute/all-pricing). Get the [price calculator]() also. A few common instance types  are 

- g1-small 
- n1-standard-1
- n1-highmem-2
- e2-standard-2
- e2-highmem-2

```bash
gcloud compute machine-types list --zones=$ZONE,europe-west1-c
```

[Features](https://cloud.google.com/compute/docs/regions-zones#available) of each zones can be differing as well.

### Choosing a Node Image
Available Node images are

- cos 
- cosd
- ubuntu

## 1. Login to Gcloud account

```bash
docker-compose run --entrypoint '' <your-service> gcloud auth login
```

Log into your prefered gcloud account

## 2. Override Gcloud setup (Optional)

To override the entrypoint, mount a volume with the file `entrypoint.sh` with your own script

```yaml
...
volumes:
  - ./entrypoint.sh:/tmp/entrypoint.sh
...
```
Make sure that your file is executable `chmod a+x entrypoint.sh`

## 3. Work with your project isolated

Log into the contaier bash to start working on your project

```bash
docker-compose run <your-service> bash
# or
docker-compose up -d <your-service>
docker ps
docker exec -it docker-container-id bash;
```

# Cheat Sheet

### kubectl bash aliases
The docker container is configured with a number of aliases to reduce typing. 
```bash
k     # kubectl $@
ka    # kubectl apply $@
kg    # kubectl get $@
kgn   # kubectl get nodes
kgnw  # kubectl get nodes -o wide
kgnl  # kubectl get nodes --show-labels
kgp   # kubectl get pods
kgs   # kubectl get svc
kgd   # kubectl get deployments
kcf   # kubectl create -f $@
kd    # kubectl delete $@
kdes  # kubectl describe $@
kdf   # kubectl delete -f $@
kaf   # kubectl apply -f $@
kgpa  # kubectl get pods --all-namespaces
kssh  # kubectl exec -it $@ -- /bin/sh 
kbash # kubectl exec -it $@ -- /bin/bash

```

### Create cluster

```bash
gcloud container get-server-config # get available cluster versions
gcloud container clusters list
gcloud container clusters create $CLUSTER_NAME $CLUSTER_FLAGS \
    --num-nodes $REGULAR_NODES \
    --machine-type $NODE_TYPE \
    --image-type $NODE_IMAGE

kubectl version # check kubernetes versions
```
Create an auto-upgraded cluster;
```bash
gcloud container clusters create $CLUSTER_NAME $CLUSTER_FLAGS \
    --num-nodes $REGULAR_NODES \
    --machine-type $NODE_TYPE \
    --image-type $NODE_IMAGE \
    --maintenance-window $CLUSTER_MAINTENANCE_WINDOW \
    --release-channel $CLUSTER_RELEASE_CHANNEL
```
During upgrades, your workloads continue to run, but you cannot deploy new workloads, modify existing workloads, or make other changes to the cluster's configuration until the upgrade is complete.

Reference links:
- https://cloud.google.com/kubernetes-engine/docs/concepts/cluster-upgrades
- https://cloud.google.com/kubernetes-engine/docs/concepts/release-channels
- https://cloud.google.com/kubernetes-engine/docs/how-to/maintenance-windows-and-exclusions

### Create node-pool with preemptible VMs

```bash
gcloud container node-pools create $POOL_NAME --cluster=$CLUSTER_NAME \
  --num-nodes=$PREEMPTIBLE_NODES --preemptible \
  --machine-type $NODE_TYPE \
  --enable-autoscaling --min-nodes=1 --max-nodes=$PREEMPTIBLE_NODES_MAX \
  --zone=$ZONE
```

#### Prevent pods from running on a preemptible instance
```yaml
affinity:
  nodeAffinity:
    requiredDuringSchedulingIgnoredDuringExecution:
      nodeSelectorTerms:
      - matchExpressions:
        - key: cloud.google.com/gke-preemptible
          operator: DoesNotExist
```
Or run on preemptible instances if available;
```yaml
affinity:
  nodeAffinity:
    preferredDuringSchedulingIgnoredDuringExecution:
    - weight: 100
      preference:
        matchExpressions:
        - key: cloud.google.com/gke-preemptible
          operator: Exists
```
### View configurations

```bash
gcloud config list
gcloud config configurations list
gcloud projects list
```

### Authorize Kubectl

```bash
gcloud container clusters get-credentials $CLUSTER_NAME --zone $ZONE
```

### [Set default cluster storage class](https://cloud.google.com/anthos/gke/docs/on-prem/how-to/default-storage-class)
The default storageclass is identified by the annotation `storageclass.kubernetes.io/is-default-class: "true"`.
The `standard` storageclass created during cluster creation is set as default.
To set a different storageclass as default, remove the annotation from `standard` and assign to the new storageclass.

```yaml
apiVersion: storage.k8s.io/v1beta1
kind: StorageClass
metadata:
  name: fast
  annotations:
    storageclass.kubernetes.io/is-default-class: "true"
provisioner: kubernetes.io/gce-pd
parameters:
  type: pd-ssd
allowVolumeExpansion: true
```

```bash
kubectl edit storageclass standard # edit and delete annotation
kubectl apply -f some-storage-class-manifest.yaml
```
### Delete cluster

```bash
gcloud container clusters delete $CLUSTER_NAME
```

### Create Static IP

global ip

```bash
gcloud compute addresses create <ip-name> $CLUSTER_NAME --region $REGION
gcloud compute addresses create <ip-name> --global

gcloud compute addresses describe <ip-name> $CLUSTER_NAME --region $REGION
gcloud compute addresses delete <ip-name>

```

Cheatsheet [here](https://gist.github.com/pydevops/cffbd3c694d599c6ca18342d3625af97)

## Calico CNI

https://kubernetes.io/docs/tasks/administer-cluster/network-policy-provider/calico-network-policy/
