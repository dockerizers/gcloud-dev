#!/bin/bash
set -e

echo "to log in first time run"
echo "docker-compose run --entrypoint '' <your-service> gcloud auth login"

echo "Setting project $PROJECT"
gcloud config set core/project $PROJECT

echo "Setting default timezone $ZONE"
gcloud config set compute/zone $ZONE

echo "Displaying gcloud configurations"
gcloud config list

echo "Checking cluster availability"
ALL_CLUSTER=$(gcloud container clusters list)

if [[ "$ALL_CLUSTER" == *"$CLUSTER_NAME"* ]]; then
	echo "Cluster exists. Using cluster $CLUSTER_NAME"
	echo "Setting up gcloud and kubectl credentials"
	gcloud container clusters get-credentials $CLUSTER_NAME --zone $ZONE
else
	echo "Cluster does not exist. We shall create cluster $CLUSTER_NAME now"
	gcloud container clusters create $CLUSTER_NAME $CLUSTER_FLAGS \
    --enable-ip-alias \
    --enable-autoupgrade \
    --enable-autorepair \
    --enable-shielded-nodes \
    --enable-autoscaling \
		--num-nodes $REGULAR_NODES \
		--machine-type $NODE_TYPE \
    --image-type $NODE_IMAGE \
    --min-nodes=$REGULAR_NODES_MIN \
    --max-nodes=$REGULAR_NODES_MAX \
    --maintenance-window $CLUSTER_MAINTENANCE_WINDOW \
    # --release-channel $CLUSTER_RELEASE_CHANNEL \ # Available in gcloud beta container ...

  if [[ -n "$PREEMPTIBLE_POOL_NAME" ]]; then
    echo "Adding preemptible VMs node pool: $PREEMPTIBLE_POOL_NAME"
    gcloud container node-pools create $PREEMPTIBLE_POOL_NAME --cluster=$CLUSTER_NAME \
      --machine-type $NODE_TYPE \
      --enable-autoscaling \
      --enable-autoupgrade \
      --enable-autorepair \
      --preemptible \
      --num-nodes=$PREEMPTIBLE_NODES \
      --min-nodes=$PREEMPTIBLE_NODES_MIN \
      --max-nodes=$PREEMPTIBLE_NODES_MAX
  fi
fi

exec "$@"