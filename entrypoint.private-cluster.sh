#!/bin/bash
# https://medium.com/google-cloud/using-cloud-nat-with-gke-cluster-c82364546d9e
# https://cloud.google.com/kubernetes-engine/docs/how-to/private-clusters
set -e

echo "to log in first time run"
echo "docker-compose run --entrypoint '' <your-service> gcloud auth login"

echo "Setting project $PROJECT"
gcloud config set core/project $PROJECT

echo "Setting default timezone $ZONE"
gcloud config set compute/zone $ZONE

echo "Displaying gcloud configurations"
gcloud config list

echo "Checking cluster availability"
ALL_CLUSTER=$(gcloud container clusters list)

if [[ "$ALL_CLUSTER" == *"$CLUSTER_NAME"* ]]; then
	echo "Cluster exists. Using cluster $CLUSTER_NAME"
	echo "Setting up gcloud and kubectl credentials"
	gcloud container clusters get-credentials $CLUSTER_NAME --zone $ZONE
else
	echo "Cluster does not exist. We shall create private cluster $CLUSTER_NAME now"
    gcloud container clusters create $CLUSTER_NAME $CLUSTER_FLAGS \
            --num-nodes $REGULAR_NODES \
            --machine-type $NODE_TYPE \
            --image-type $NODE_IMAGE \
            --enable-ip-alias \
            --enable-private-nodes \
            --master-ipv4-cidr "172.15.0.0/28" \
            --no-enable-master-authorized-networks
fi

echo "Checking router availability"
ALL_ROUTERS=$(gcloud compute routers list)
if [[ "$ALL_ROUTERS" == *"$NAT_ROUTER"* ]]; then
	echo "Router exists. Using router $NAT_ROUTER"
else
    # Create a NAT configuration using Cloud Router
    echo "Creating a Cloud Router"
    gcloud compute routers create $NAT_ROUTER \
        --network default \
        --region $REGION
fi

echo "Checking NAT availability"
ALL_NATS=$(gcloud compute routers nats list --router=$NAT_ROUTER --region=$REGION )
if [[ "$ALL_NATS" == *"$NAT_CONFIG"* ]]; then
	echo "Nat Gateway exists. Using NAT $NAT_CONFIG"
else
    echo "Adding a NAT configuration to the router"
    gcloud compute routers nats create $NAT_CONFIG \
        --router-region $REGION \
        --router $NAT_ROUTER \
        --nat-all-subnet-ip-ranges \
        --nat-external-ip-pool=$NAT_IP_NAME
        # --auto-allocate-nat-external-ips
fi

# MASTER_AUTHORIZATION_NETWORKS=$(wget -qO- ifconfig.me)/32
# echo "Configuring kubectl access via Master Authorization Networks from $MASTER_AUTHORIZATION_NETWORKS"
# gcloud container clusters update $CLUSTER_NAME \
#     --enable-master-authorized-networks \
#     --enable-master-global-access \
#     --master-authorized-networks $MASTER_AUTHORIZATION_NETWORKS

echo "Startup Complete"

exec "$@"