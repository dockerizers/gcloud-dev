# Use bash-completion, if available
[[ $PS1 && -f /usr/share/bash-completion/bash_completion ]] && \
    . /usr/share/bash-completion/bash_completion
# add krew to $PATH
export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"
# kubectl completion
source ~/kube_completion.bash
# gcloud completion
source /google-cloud-sdk/completion.bash.inc
